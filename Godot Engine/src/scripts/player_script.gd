extends KinematicBody

export var gravity = -99.0
export var speed = 7.0
export var accel = 9.0
var sprint
var sprinting = false
export var sprint_vel = 8.0
var crouching = false
export var jump_pow = 32.0

var vel = Vector3()
var dir = Vector3()

var move_state
enum { STANDING, WALKING, JUMPING, FALLING, LANDING }

var mouse_sens = 0.4

onready var rot_help = $Rotation_Helper
onready var cam = $Rotation_Helper/Camera
onready var animation = $AnimationPlayer

func _ready() :
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta) :
	proc_input() #process input
	proc_move(delta) #process movement

# --- Handling Input ---
func proc_input() :
	
	var cam_tform = cam.get_global_transform() #camera transform
	var input_move_vec = Vector2()
	
	#- Standing
	if move_state == WALKING and input_move_vec == Vector2.ZERO :
		move_state = STANDING
	
	#- Walking
	if Input.is_action_pressed("ui_up") :
		input_move_vec.y -= 1
		if move_state != FALLING and move_state != LANDING and not crouching :
			move_state = WALKING
			animation.play("Walking")
	if Input.is_action_pressed("ui_down") :
		input_move_vec.y += 1
		if move_state != FALLING and move_state != LANDING and not crouching :
			move_state = WALKING
			animation.play("Walking")
	if Input.is_action_pressed("ui_left") :
		input_move_vec.x -= 1
		if move_state != FALLING and move_state != LANDING and not crouching :
			move_state = WALKING
			animation.play("Walking")
	if Input.is_action_pressed("ui_right") :
		input_move_vec.x += 1 
		if move_state != FALLING and move_state != LANDING and not crouching :
			move_state = WALKING
			animation.play("Walking")
	
	input_move_vec = input_move_vec.normalized()
	#direction
	dir = cam_tform.basis.z * input_move_vec.y
	dir += cam_tform.basis.x * input_move_vec.x
	
	#sprinting
	if Input.is_action_pressed("sprint") and not crouching :
		sprint = sprint_vel
		sprinting = true
	else :
		sprint = 0.0
		sprinting = false
	
	#- Jumping
	if is_on_floor() and Input.is_action_just_pressed("jump") and not crouching :
		move_state = JUMPING
		vel.y = jump_pow
	
	#- Falling
	if vel.y < -8.0 :
		move_state = FALLING
	
	#- Landing
	if move_state == FALLING and is_on_floor() :
		crouching = false
		move_state = LANDING
		animation.play("Landing")
		yield(animation, "animation_finished")
		move_state = STANDING
	
	#crouching
	if Input.is_action_just_pressed("crouch") and is_on_floor() :
		crouching = true
		animation.play("Crouching")
	if Input.is_action_just_released("crouch") and is_on_floor() and crouching :
		animation.play_backwards("Crouching")
		yield(animation, "animation_finished")
		crouching = false
	
	#- Cursor managing
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

# --- Processing Movement ---
func proc_move(delta) :
	
	#vertical direction
	dir.y = 0
	dir = dir.normalized()
	
	#considering gravity
	vel.y += delta * gravity
	
	#- Movement
	vel = vel.linear_interpolate( (speed + sprint) * dir, accel * delta )
	vel = move_and_slide(vel, Vector3(0, 1, 0))

func _input(event) :
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED :
		rot_help.rotate_x(deg2rad(event.relative.y * mouse_sens * -1))
		self.rotate_y(deg2rad(event.relative.x * mouse_sens * -1))
	
	var rot_limiter = rot_help.rotation_degrees
	#avoiding upside down rotation
	rot_limiter.x = clamp(rot_limiter.x, -60, 60)
	rot_help.rotation_degrees = rot_limiter
