> <p>Godot version: <b>3.2.3</b></p>

<p>The <i>src</i> directory contains the player scene and the relative script ready to be implemented in your project<br>
Set values (like gravity, rotation_degrees or collision_shapes size) according to your needs.</p>

<p>Remember to set Input Map values from project settings.</p>

<sub>
The starting point of this build is the Official Godot Documentation.<br>
Be sure to take a look at it [here](https://docs.godotengine.org/it/stable/tutorials/3d/fps_tutorial/index.html)
</sub>
